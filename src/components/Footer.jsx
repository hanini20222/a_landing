/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';

function Footer() {
  return (
    <footer className="footer">
      <div className="footer__wrapper">
        <div className="footer__text">
          <p>
            Nulla sit amet est. Praesent metus tellus, elementum eu, semper a, adipiscing nec,
            purus. Cras risus ipsum, faucibus ut, ullamcorper id, varius ac, leo. Suspendisse
            feugiat. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi.
          </p>
          <p>
            Praesent nec nisl a purus blandit viverra. Praesent ac massa at ligula laoreet iaculis.
            Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit.
          </p>
        </div>
        <ul className="footer__menu">
          <li>
            <a href="#">Home</a>
          </li>
          <li>
            <a href="#">Service</a>
          </li>
          <li>
            <a href="#">Works</a>
          </li>
          <li>
            <a href="#">About me</a>
          </li>
          <li>
            <a href="#">Contact</a>
          </li>
        </ul>
        <div className="footer__copywrite">
          &#169;
          <strong>DEMO</strong>
          SITE All right reserved
        </div>
      </div>
    </footer>
  );
}
export default Footer;
