import React from 'react';
import CustomList from './CustomList';

function MainSection() {
  return (
    <section className="main">
      <div className="main__wrapper">
        <div className="main__web">
          <h3>Web development</h3>
          <p>
            Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet
            iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer
            eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc.
            Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui.
          </p>
          <p>
            Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet,
            nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus
            nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque.
            Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi.
          </p>
        </div>
        <div className="main__wireframe">
          <h3>Wireframe</h3>
          <p>
            Phasellus blandit leo ut odio. Maecenas ullamcorper, dui et placerat feugiat, eros pede
            varius nisi, condimentum viverra felis nunc et lorem.
          </p>
          <div className="main__subsection">
            <div>
              <h4>Custom List</h4>
              <h5>Some heading</h5>
              <CustomList />
            </div>
            <div>
              <h4>Web Development</h4>
              <p>
                Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci.
                Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
export default MainSection;
