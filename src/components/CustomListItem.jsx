/* eslint-disable react/prop-types */
import React from 'react';

const CustomListItem = ({ data }) => {
  const items = data.map(item => {
    return <li key={item.id}>{item.title}</li>;
  });
  return <ul>{items}</ul>;
};
export default CustomListItem;
