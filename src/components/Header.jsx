/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';

function Header() {
  return (
    <header className="header">
      <div className="header__wrapper">
        <ul className="header__top-menu">
          <li>
            <a href="#">Menu</a>
          </li>
          <li>
            <a href="#">Some text</a>
          </li>
          <li>
            <a href="#">Another item</a>
          </li>
          <li>
            <a href="#">One more</a>
          </li>
          <li>
            <a href="#">And last one</a>
          </li>
        </ul>
        <h1 className="header__logo">
          <strong>DEMO</strong>
          SITE
        </h1>
        <nav>
          <ul className="header__main-menu">
            <li>
              <a href="#">Home</a>
            </li>
            <li>
              <a href="#">Service</a>
            </li>
            <li className="header__main-menu--works">
              <a href="#">Works</a>
              <div className="header__works">
                <a href="#">All</a>
                <a href="#">Graphic</a>
                <a href="#">Design</a>
                <a href="#">Logo</a>
                <a href="#">Website</a>
              </div>
            </li>
            <li>
              <a href="#">About me</a>
            </li>
            <li>
              <a href="#">Contact</a>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
}
export default Header;
