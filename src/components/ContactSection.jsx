/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';

function ContactSection() {
  return (
    <section className="contact">
      <div className="contact__wrapper">
        <h2 className="contact__title">Contact</h2>
        <div className="contact__text">
          <p>
            In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor
            malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a,
            auctor quis, euismod ut, mi. Aenean viverra rhoncus pede.
          </p>
          <p>
            Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
            egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est.
            Phasellus magna.
          </p>
          <p>
            Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Sed
            hendrerit. Morbi ac felis. Nunc egestas, augue at pellentesque laoreet, felis eros
            vehicula leo, at malesuada velit leo quis pede.
          </p>
        </div>
        <div className="contact__form">
          <label htmlFor="name">Name</label>
          <input id="name" name="name" type="text" />

          <label htmlFor="email">Email</label>
          <input id="email" name="email" type="text" />

          <label htmlFor="message">Message</label>
          <textarea id="message" name="message" cols="30" rows="10" />

          <button type="button">Send</button>
        </div>
      </div>
    </section>
  );
}
export default ContactSection;
