import React, { useState, useEffect } from 'react';
import axios from 'axios';
import CustomListItem from './CustomListItem';

const getRandomFour = data => {
  const res = [];
  for (let i = 0; i < 4; i += 1) {
    res.push(data[Math.floor(Math.random() * data.length)]);
  }
  return res;
};

const CustomList = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      const result = await axios('https://jsonplaceholder.typicode.com/posts');

      setData(getRandomFour(result.data));
    };

    fetchData();
  }, []);

  return <CustomListItem data={data} />;
};
export default CustomList;
