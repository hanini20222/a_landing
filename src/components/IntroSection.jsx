/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';

function IntroSection() {
  return (
    <section className="intro">
      <div className="intro__wrapper">
        <div className="intro__welcome">
          <h2>WELCOME</h2>
          <p>
            Sed amet consectetur adipisicing elit. Culpa quae dolorem recusandae nam maiores illo
            consectetur nesciunt, delectus repudiandae saepe, minus a sint aut ex incidunt. Repellat
            assumenda architecto voluptatibus!
          </p>
          <div>
            <a className="intro__button" href="#">
              Learn more
            </a>
          </div>
        </div>
        <div className="intro__article">
          <img
            srcSet="
                img/4.jpg  350w,
                img/3.jpg 700w,
                img/2.jpg 1400w,
                img/1.jpg 2100w
              "
            sizes="
              (max-width: 374px) 350px,
              (max-width: 500px) 476px,
              (max-width: 750px) 670px,
              700px
              "
            src="img/4.jpg"
            alt="UI / UX Design"
          />
          <h3>UI/UX Design</h3>
          <p>
            Etiam rhoncus. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
            Maecenas nec odio et ante tincidunt tempus.
          </p>
          <p>
            Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet
            adipiscing sem neque sed ipsum.
          </p>
        </div>
      </div>
    </section>
  );
}
export default IntroSection;
