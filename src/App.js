import React from 'react';

import Header from './components/Header';
import IntroSection from './components/IntroSection';
import MainSection from './components/MainSection';
import ContactSection from './components/ContactSection';
import Footer from './components/Footer';

import './sass/main.scss';

function App() {
  return (
    <>
      <Header />
      <IntroSection />
      <MainSection />
      <ContactSection />
      <Footer />
    </>
  );
}

export default App;
